package sample.wrk;

import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;
import sample.model.Annotation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Worker {

    private final static String FILE_PATH = "data.json";
    private List<Annotation> annotations;

    public Worker() {
        this.readAnnotations();
    }

    private void readAnnotations() {
        this.annotations = new ArrayList<>();
        try {
            File file = new File(FILE_PATH);
            if (!file.exists()) return;
            BufferedReader br = new BufferedReader(new FileReader(file));

            Gson gson = new Gson();
            Type jsonType = new TypeToken<List<Annotation>>(){}.getType();
            this.annotations = gson.fromJson(br, jsonType);
            if (this.annotations == null) this.annotations = new ArrayList<>();
            this.annotations.sort(this::compare);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    private void writeAnnotations() {
        try {
            File file = new File(FILE_PATH);
            if (!file.exists()) file.createNewFile();
            FileWriter writer = new FileWriter(file, Charset.forName("UTF8"));

            Gson gson = new Gson();
            this.annotations.sort(this::compare);
            writer.write(gson.toJson(this.annotations));
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public boolean addAnnotation(Annotation annotation) {
        // When we're adding new annotation, it shouldn't overlap others.
        if (isOverlappingAnotherAnnotation(annotation)) return false;
        this.annotations.add(annotation);
        this.annotations.sort(this::compare);
        return true;
    }

    /**
     * Is overlapping another annotations ?
     * @param annotation targeted
     * @param ignoreItself if another annotation has the exact timestamp it should be ignored (modify annotation case)
     * @return false if it isn't overlapping, true if it's overlapping another annotation.
     */
    public boolean isOverlappingAnotherAnnotation(Annotation annotation, boolean ignoreItself) {
        Annotation closestAnnotationBefore = findClosestAnnotation(annotation.timestamp, TimeDirection.BEFORE, ignoreItself);
        Annotation closestAnnotationAfter = findClosestAnnotation(annotation.timestamp, TimeDirection.AFTER, ignoreItself);
        if (closestAnnotationBefore != null && closestAnnotationAfter != null) {
            if (annotation.timestamp < closestAnnotationBefore.timestamp + closestAnnotationBefore.duration) return true;
            if (annotation.timestamp + annotation.duration > closestAnnotationAfter.timestamp) return true;
        } else if (closestAnnotationBefore != null && closestAnnotationAfter == null){
            if (annotation.timestamp < closestAnnotationBefore.timestamp + closestAnnotationBefore.duration) return true;
        } else if (closestAnnotationBefore == null && closestAnnotationAfter != null){
            if (annotation.timestamp + annotation.duration > closestAnnotationAfter.timestamp) return true;
        }
        return false;
    }

    public boolean isOverlappingAnotherAnnotation(Annotation annotation) {
        return isOverlappingAnotherAnnotation(annotation, false);
    }

    /**
     * Remove annotation from list
     * @param annotation targeted
     * @return true if successfully removed
     */
    public boolean deleteAnnotation(Annotation annotation) {
        return this.annotations.remove(annotation);
    }

    /**
     * Edit annotation from list
     * @param annotation targeted
     * @return true if successfully edited
     */
    public boolean editAnnotation(Annotation annotation) {
        int index = this.annotations.indexOf(annotation);
        if (index != -1) {
            if (isOverlappingAnotherAnnotation(annotation, true)) return false;
            this.annotations.set(index, annotation);
            return true;
        }
        return false;
    }

    /**
     * Finds the closest annotation before the provided timestamp.
     * @param timestamp to target
     * @param ignoreItself if another annotation has the exact timestamp it should be ignored (modify annotation case)
     * @return null if nothing found
     */
    private Annotation findClosestAnnotation(double timestamp, TimeDirection timeDirection, boolean ignoreItself) {
        Annotation result = null;
        for(Annotation annotation : this.annotations) {
            if (annotation.timestamp < timestamp) {
                if (timeDirection == TimeDirection.BEFORE) result = annotation;
            } else {
                if (ignoreItself && annotation.timestamp == timestamp) continue;
                if (timeDirection == TimeDirection.AFTER) result = annotation;
                break;
            }
        }
        return result;
    }

    private Annotation findClosestAnnotation(double timestamp, TimeDirection timeDirection) {
        return findClosestAnnotation(timestamp, timeDirection, false);
    }

    /**
     * Used to sort annotations list (depending on timestamp).
     * Sorting is useful to use iterator (for IHM display)
     **/
    private int compare(Annotation a1, Annotation a2) {
        if (a1.timestamp == a2.timestamp) return 0;
        else if (a1.timestamp > a2.timestamp) return 1;
        else return -1;
    }

    private enum TimeDirection {
        BEFORE,
        AFTER
    }

    /**
     * Get annotation from specific timestamp targeted
     * @param timestampTargeted targeted
     * @return null if no annotation found in timestamp targeted, else return annotation founded
     */
    public Annotation getAnnotationFromTimestamp(double timestampTargeted) {
        Annotation closestAnnotation = this.findClosestAnnotation(timestampTargeted, TimeDirection.BEFORE);
        if (closestAnnotation == null) return null;
        if (closestAnnotation.timestamp + closestAnnotation.duration >= timestampTargeted) return closestAnnotation;
        return null;
    }

    /**
     * Save all modification happened about the annotations.
     */
    public void saveAnnotations() {
        this.writeAnnotations();
    }

    /**
     * @return all annotations sorted (by timestamp).
     */
    public List<Annotation> getAnnotations() {
        return annotations;
    }
}
