package sample.ihm;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.*;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import sample.ctrl.Controller;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import sample.model.Annotation;

import java.io.File;
import java.util.Optional;

public class Ihm extends BorderPane {

    public final static String TITLE_IMG = "/sample/resources/volume.png";

    private final Controller ctrl;
    private final Application app;

    // Menu
    private MenuBar mBar = new MenuBar();
    private Menu mMenu = new Menu("_Menu");
    private MenuItem mFullscreen = new MenuItem("Fullscreen");
    private SeparatorMenuItem mSeparator = new SeparatorMenuItem();
    private MenuItem mImport = new MenuItem("_Import");
    private MenuItem mSave = new MenuItem("_Save");
    private SeparatorMenuItem mSeparator2 = new SeparatorMenuItem();
    private MenuItem mQuit = new MenuItem("_Quit");

    // Annotation
    private HBox hbAnnotation = new HBox();
    private TextField tfAnnotation = new TextField();
    private Button btnAdd = new Button("Add");
    private Button btnModify = new Button("Modify");
    private Button btnDelete = new Button("Delete");
    private Spinner<Double> spDuration = new Spinner<>(0.5,5,1,0.1);
    private SpinnerValueFactory.DoubleSpinnerValueFactory factory = new SpinnerValueFactory.DoubleSpinnerValueFactory(0.5,5,1,0.1);

    // List Annotation
    private VBox vbAnnotation = new VBox();
    private ListView<Annotation> lstAnnotation = new ListView<>();

    // Player
    private VBox player = new VBox();
    private Label lblNoPlayer = new Label("Import a video via \"Menu > Import\".");
    private HBox controller = new HBox();
    private Slider sldTimeline = new Slider();
    private Button btnPlay = new Button("_Play / Stop");
    private MediaPlayer mediaPlayer;
    private MediaView mediaView = new MediaView();
    private Slider sldVolume = new Slider();
    private ImageView imgVolume = new ImageView();
    private Label lblTime = new Label("0:00:00");

    // Import file
    private FileChooser fileChooser = new FileChooser();
    private File homeDir = new File(System.getProperty("user.home"));
    private File selectedFile;

    // Footer
    private VBox vbFooter = new VBox();
    private Separator separator = new Separator(Orientation.HORIZONTAL);
    private Label lblTitleFooter = new Label("Annotations Management");

    Alert alertClosing = new Alert(Alert.AlertType.WARNING);
    private ButtonType btnOk = new ButtonType("Yes");
    private ButtonType btnCancel = new ButtonType("No", ButtonBar.ButtonData.CANCEL_CLOSE);

    public Ihm(Application app, Controller ctrl) {
        // MVC
        this.ctrl = ctrl;
        this.app = app;

        // Menu
        this.mSave.setAccelerator(KeyCombination.keyCombination("Ctrl+S"));
        this.mImport.setAccelerator(KeyCombination.keyCombination("Ctrl+I"));
        this.mQuit.setAccelerator(KeyCombination.keyCombination("Ctrl+Q"));
        this.mFullscreen.setAccelerator(KeyCombination.keyCombination("Ctrl+F"));
        this.mMenu.getItems().addAll(mFullscreen, mSeparator, mImport, mSave, mSeparator2, mQuit);
        this.mBar.getMenus().add(mMenu);
        this.mBar.setStyle("-fx-background-color: #fefefe");
        this.mBar.setPadding(new Insets(8));
        this.setTop(mBar);

        // Import
        this.fileChooser.setTitle("Take a video");
        this.fileChooser.setInitialDirectory(homeDir);
        this.fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("select your media (*.mp4)", "*.mp4"));

        // Annotation
        this.spDuration.setMaxWidth(70);
        this.spDuration.isResizable();
        this.factory.setWrapAround(true);
        this.spDuration.setValueFactory(factory);
        this.spDuration.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);

        this.btnModify.setDisable(true);
        this.btnDelete.setDisable(true);
        this.hbAnnotation.getChildren().addAll(
                new Label("Message: "), tfAnnotation,
                new Label("Duration (seconds): "),
                spDuration, btnAdd, btnModify, btnDelete);
        this.hbAnnotation.setAlignment(Pos.CENTER);
        this.hbAnnotation.setSpacing(8);
        this.hbAnnotation.setPadding(new Insets(12));
        this.hbAnnotation.setDisable(true);

        // List Annotation
        this.vbAnnotation.getChildren().addAll(lstAnnotation);
        this.vbAnnotation.setFillWidth(true);
        this.vbAnnotation.setPadding(new Insets(12));
        this.lstAnnotation.prefHeightProperty().bind(this.heightProperty().subtract(this.mBar.getHeight()).subtract(this.vbFooter.getHeight()));

        // Add to list annotations all saved annotations
        ObservableList<Annotation> annotations = FXCollections.observableArrayList(this.ctrl.getAnnotations());
        this.lstAnnotation.setItems(annotations);
        this.lstAnnotation.setDisable(true);

        // Volume
        sldVolume = new Slider(0.0, 1.0, 1.0);
        sldVolume.setPrefWidth(10);
        // Initialisation titleImg
        Image image = new Image(getClass().getResource(TITLE_IMG).toString());
        imgVolume.setImage(image);
        imgVolume.setFitWidth(25);
        imgVolume.setPreserveRatio(true);

        // Font
        this.lblTitleFooter.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.REGULAR, 20));
        this.lstAnnotation.setBorder(new Border(
                new BorderStroke(
                        Color.GRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1), new Insets(0))
        ));

        this.controller.getChildren().addAll(btnPlay, sldTimeline, lblTime, imgVolume, sldVolume);
        this.controller.setAlignment(Pos.CENTER);
        this.controller.setSpacing(8);
        this.controller.setDisable(true);
        this.lblNoPlayer.setStyle("-fx-font: 20 arial;");
        this.player.getChildren().addAll(lblNoPlayer, mediaView, controller);
        this.player.setAlignment(Pos.CENTER);
        this.player.setPadding(new Insets(8));
        this.player.setSpacing(8);
        this.mediaView.setMediaPlayer(mediaPlayer);
        this.mediaView.setPreserveRatio(true);
        this.lstAnnotation.setStyle("-fx-highlight-fill: lightblue; -fx-highlight-text-fill: firebrick; -fx-font: 12 arial;");

        // Footer
        this.vbFooter.setAlignment(Pos.CENTER);
        this.vbFooter.setStyle("-fx-background-color: #EFEFEF");
        this.lblTitleFooter.setPadding(new Insets(8));
        this.vbFooter.getChildren().addAll(separator, lblTitleFooter, hbAnnotation);

        this.setCenter(player);
        this.setBottom(vbFooter);
        this.setRight(vbAnnotation);

        // Define alertSizeClosingWindow
        alertClosing.setTitle("Attention: Loss of data !!!");
        alertClosing.setContentText("Did you save your modifications before exiting?");
        alertClosing.getButtonTypes().setAll(btnOk, btnCancel);

        createListeners();
        createBindings();
    }

    /**
     * Create all listeners
     */
    private void createListeners() {
        /* Import video */
        mImport.setOnAction(event -> {
            this.selectedFile = fileChooser.showOpenDialog(this.ctrl.getIhm());
            if (selectedFile != null) {
                Media newMedia = new Media(selectedFile.toURI().toString());
                this.mediaPlayer = new MediaPlayer(newMedia);
                this.mediaPlayer.setOnReady(() -> {
                    this.hbAnnotation.setDisable(false);
                    this.lstAnnotation.setDisable(false);
                    this.lblNoPlayer.setVisible(false);
                    this.sldTimeline.setMax(this.mediaPlayer.getTotalDuration().toMillis());
                    // Add listener to the new player
                    this.mediaPlayer.currentTimeProperty().addListener((obs, oldValue, newValue) -> {
                        sldTimeline.setValue(newValue.toMillis()); // Define current time on the slider
                        this.lstAnnotation.getSelectionModel().select(
                               this.ctrl.getAnnotationFromTimestamp(
                                       this.mediaPlayer.getCurrentTime().toMillis())); // highlights the time annotation of the video
                    });
                });
                this.mediaPlayer.setOnEndOfMedia(() -> mediaPlayer.seek(Duration.ZERO));
                this.mediaView.setMediaPlayer(mediaPlayer);

                // Responsive player
                this.mediaView.fitWidthProperty().bind(this.widthProperty()
                        .subtract(this.lstAnnotation.widthProperty())
                        .subtract(40));
                this.mediaView.fitHeightProperty().bind(this.heightProperty()
                        .subtract(this.vbFooter.getHeight())
                        .subtract(this.mBar.getHeight())
                        .subtract(50));

                // Make controller usable
                this.controller.setDisable(false);
                this.mediaPlayer.play();
                this.btnPlay.setText("Pause");

                // Time label
                lblTime.textProperty().bind(
                        Bindings.createStringBinding(() -> {
                                    Duration time = this.mediaPlayer.getCurrentTime();
                                    return String.format("%4d:%02d:%04.1f",
                                            (int) time.toHours(),
                                            (int) time.toMinutes() % 60,
                                            time.toSeconds() % 3600);
                                },
                                this.mediaPlayer.currentTimeProperty()));
            }
        });


        /* Used to adapt volume */
        sldVolume.valueProperty().addListener(ov -> {
            if (sldVolume.isValueChanging()) {
                mediaPlayer.setVolume(sldVolume.getValue());
            }
        });

        /* Used to trigger fullscreen mode */
        mFullscreen.setOnAction(event -> {
            ((Stage) this.mediaView.getScene().getWindow()).setFullScreen(true);
        });

        /* Used to save annotations */
        mSave.setOnAction(event -> {
            this.ctrl.saveAnnotation();
        });

        /* Used to add annotations */
        btnAdd.setOnAction(event -> {
            Annotation annotation = new Annotation(sldTimeline.getValue(), this.tfAnnotation.getText(), this.spDuration.getValue() * 1000);
            if (!this.ctrl.createAnnotation(annotation)) {
                Alert dialog = new Alert(Alert.AlertType.ERROR);
                dialog.setTitle("Error");
                dialog.setHeaderText("Cannot create annotation");
                dialog.setContentText("Your annotation is overlapping another!");
                dialog.showAndWait();
            } else {
                ObservableList<Annotation> annotations = FXCollections.observableArrayList(this.ctrl.getAnnotations());
                this.lstAnnotation.getItems().clear();
                this.lstAnnotation.setItems(annotations);
            }
        });

        /* Used to play video */
        btnPlay.setOnAction(event -> {
            if (this.mediaPlayer != null) {
                switch (this.mediaPlayer.getStatus()) {
                    case UNKNOWN:
                        break;
                    case PLAYING:
                        this.mediaPlayer.pause();
                        this.btnPlay.setText("Resume");
                        break;
                    case STOPPED:
                    case PAUSED:
                        this.btnPlay.setText("Pause");
                        this.mediaPlayer.play();
                        break;
                }
            }
        });

        /* Used to edit annotation */
        btnModify.setOnAction(event -> {
            Annotation annotation = this.lstAnnotation.getSelectionModel().getSelectedItem();
            int index = this.lstAnnotation.getItems().indexOf(annotation);
            annotation.duration = this.spDuration.getValue() * 1000;
            annotation.annotation = this.tfAnnotation.getText();
            if (this.ctrl.editAnnotation(annotation)) {
                this.lstAnnotation.getItems().set(index, annotation);
            }
        });

        /* Used to delete annotation */
        btnDelete.setOnAction(event -> {
            Annotation annotation = this.lstAnnotation.getSelectionModel().getSelectedItem();
            if (this.ctrl.deleteAnnotation(annotation)) {
                this.lstAnnotation.getItems().remove(annotation);
            }
        });

        /* Update annotation fields depending on selected annotation */
        lstAnnotation.getSelectionModel().selectedItemProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue != null) {
                this.tfAnnotation.setText(newValue.annotation);
                this.spDuration.getValueFactory().setValue(newValue.duration / 1000);
            }
            if(this.lstAnnotation.getSelectionModel().getSelectedItem()== null){
                this.btnModify.setDisable(true);
                this.btnDelete.setDisable(true);
            }else{
                this.btnModify.setDisable(false);
                this.btnDelete.setDisable(false);
            }
        });

        /* Used to seek video */
        sldTimeline.setOnMouseClicked(event -> {
            if (mediaPlayer != null) {
                mediaPlayer.seek(Duration.millis(sldTimeline.getValue()));
            }
        });

        /* Used to quit the application */
        mQuit.setOnAction(event -> {
            Optional<ButtonType> choice = alertClosing.showAndWait();
            if (choice.get() == btnOk){
                Platform.exit();
            }

        });

    }

    /**
     * Create all bindings
     */
    private void createBindings() {
    }
}
