package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.ctrl.Controller;
import sample.ihm.Ihm;
import sample.wrk.Worker;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        // mvc
        Worker wrk = new Worker();
        Controller ctrl = new Controller(primaryStage, wrk);
        Ihm ihm = new Ihm(this, ctrl);

        // primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.setScene(new Scene(ihm, 700, 500));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
