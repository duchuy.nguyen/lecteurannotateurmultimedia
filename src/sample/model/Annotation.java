package sample.model;

public class Annotation {
    public double timestamp;
    public double duration;
    public String annotation;

    public Annotation(double timestamp, String annotation, double duration) {
        this.timestamp = timestamp;
        this.annotation = annotation;
        this.duration = duration;
    }

    @Override
    public String toString() {
        return String.format("%.2f", timestamp/1000)
                + "s - "
                + String.format("%.2f", (timestamp+duration)/1000)
                + "s : " + annotation;
    }
}
