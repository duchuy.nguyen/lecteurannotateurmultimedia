package sample.ctrl;

import javafx.stage.Stage;
import sample.model.Annotation;
import sample.wrk.Worker;

import java.util.List;

public class Controller {
    private final Worker wrk;
    private final Stage ihm;

    public Controller(Stage ihm, Worker wrk) {
        this.ihm = ihm;
        this.wrk = wrk;
    }

    public Stage getIhm() {
        return ihm;
    }

    public boolean createAnnotation(Annotation annotation) {
        return this.wrk.addAnnotation(annotation);
    }

    public boolean deleteAnnotation(Annotation annotation) {
        return this.wrk.deleteAnnotation(annotation);
    }

    public boolean editAnnotation(Annotation annotation) {
        return this.wrk.editAnnotation(annotation);
    }

    public void saveAnnotation() {
        this.wrk.saveAnnotations();
    }

    public List<Annotation> getAnnotations() { return this.wrk.getAnnotations(); }

    public Annotation getAnnotationFromTimestamp(double timestampTargeted) {
        return this.wrk.getAnnotationFromTimestamp(timestampTargeted);
    }
}